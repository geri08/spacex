package menu;

import api.HistoryScreen;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {
    public void start() {
        List<String> menuItems = getMenu();
        displayMenu(menuItems);
        chooseOption(menuItems);
    }

    private void chooseOption(List<String> menuItems) {
        Scanner scanner =new Scanner(System.in);
        System.out.print("co wybierasz: ");
        int wybor = scanner.nextInt();
        if (wybor>0 && wybor<=menuItems.size()){
            switch (wybor){
                case 1:
                    System.out.println("informacje o firmie:");
                     new HistoryScreen().start();
                    break;
                case 2:
                    System.out.println("informacja o odbytych lotach:");
                    break;
                case 3:
                    System.out.println("informacja o zaplanowanych lotach: ");
                    break;
                case 4:
                    System.out.println("koniec, dziekujemy");
                    break;
            }
        }
//        else {
//            System.out.println("podałeś błędna liczbę, spróbuj jeszcze raz");
//            chooseOption(menuItems);
//        }
    }

    private void displayMenu(List<String> menuItems) {
        System.out.println("wybierz co chcesz zrobić: ");
        for (String menuItem : menuItems) {
            System.out.println(menuItem);
        }
    }

    private List<String> getMenu() {
        List<String> list = new ArrayList<String>();
        list.add("1. Pobierz informacje na temat firmy");
        list.add("2. Pobierz liste wszystkich odbytych lotow");
        list.add("3. Pobierz liste zaplanowanych lotow");
        list.add("4. koniec");
        return list;
    }
}
