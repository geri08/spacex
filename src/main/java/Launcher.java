import menu.Menu;

public class Launcher {
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.start();
    }
}
