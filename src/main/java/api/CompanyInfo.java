package api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompanyInfo {
    private String name;
    private String founder;
    private String ceo;
    private int founded;
    private int vehicles;
    private int valuation;
    @SerializedName("launch_sites")
    private int launchSites;
    @SerializedName("test_sites")
    private int testSites;
    private String cto;
    private String coo;
    @SerializedName("cto_propulsion")
    private String ctoPropulsion;
    private String summary;

    @SerializedName("headquartes")
    private List<Adress> adres;

    public  void displayInfo(){
        System.out.println("nazwa firmy : "  + name);
        System.out.println("rok założenia: " + founded);
        System.out.println("wartosc rynkowa: " + valuation);
        System.out.println("liczba pracownikow: " +summary);
    }

    public String getName() {
        return name;
    }

    public String getFounder() {
        return founder;
    }

    public String getCeo() {
        return ceo;
    }

    public int getFounded() {
        return founded;
    }

    public int getVehicles() {
        return vehicles;
    }

    public int getLaunchSites() {
        return launchSites;
    }

    public int getTestSites() {
        return testSites;
    }

    public String getCto() {
        return cto;
    }

    public String getCoo() {
        return coo;
    }

    public String getCtoPropulsion() {
        return ctoPropulsion;
    }

    public String getSummary() {
        return summary;
    }

    public List<Adress> getAdres() {
        return adres;
    }
}
