package api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    public static SpaceXApi service() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.spacexdata.com/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SpaceXApi spaceXApi = retrofit.create(SpaceXApi.class);
        return spaceXApi;
    }
}
