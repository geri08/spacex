package api;

public class Adress {

    private String address;
    private String city;
    private String state;

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }
}
