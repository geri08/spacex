package api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryScreen implements ScreenInterface {
    @Override
    public void start() {
        System.out.println("aktualna historia firmy:");
        Call<CompanyInfo> call = ApiService.service().getHistory();
        System.out.println("1");
        call.enqueue(new Callback<CompanyInfo>() {
            @Override
            public void onResponse(Call<CompanyInfo> call, Response<CompanyInfo> response) {
                System.out.println("2");
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CompanyInfo companyInfo = response.body();
                        companyInfo.displayInfo();
                    } else {
                        System.out.println(response.errorBody());
                    }
                }
            }

            @Override
            public void onFailure(Call<CompanyInfo> call, Throwable throwable) {

            }
        });
    }

    private void showResult() {
    }
}
