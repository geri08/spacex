package api;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SpaceXApi {
    @GET ("info")
    Call<CompanyInfo> getHistory();
}
